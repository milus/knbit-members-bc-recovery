package pl.edu.bit.membership.recovery.readmodel.db

import java.time.LocalDate

import com.mongodb.casbah.MongoCollection
import com.mongodb.casbah.commons.MongoDBObject
import pl.edu.bit.membership.recovery.readmodel.valueobject.{Email, UserId}

import scala.collection.JavaConverters._
import scala.collection.immutable
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.Future

import com.mongodb.casbah.Imports._

import Fields._

/**
 * @author Wojciech Milewski
 */
class UserDetailsDao(implicit db: MongoCollection) {

  def loadAll(): Future[Iterator[UserDetailsEntity]] =
    Future(db.find().map(convert))

  private def convert(o: DBObject): UserDetailsEntity = {
    UserDetailsEntity(
      UserId(o.getAs[String](id).get),
      Email(o.getAs[String](email).get),
      o.getAs[String](firstName).get,
      o.getAs[String](lastName).get,
      o.getAs[String](departmentName).get,
      o.getAs[String](indexNumber).get,
      o.getAs[String](startOfStudiesYear).get,
      LocalDate.ofEpochDay(o.getAs[Long](submitDate).get),
      LocalDate.ofEpochDay(o.getAs[Long](expirationDate).get),
      o.getAs[String](state).get
    )
  }

}

private object Fields {
  val id = "_id"
  val email = "email"
  val firstName = "firstName"
  val lastName = "lastName"
  val departmentName = "departmentName"
  val indexNumber = "indexNumber"
  val startOfStudiesYear = "startOfStudiesYear"
  val submitDate = "submitDate"
  val expirationDate = "expirationDate"
  val state = "state"

}
