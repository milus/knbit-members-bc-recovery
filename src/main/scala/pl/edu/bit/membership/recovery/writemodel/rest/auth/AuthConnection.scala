package pl.edu.bit.membership.recovery.writemodel.rest.auth

import akka.actor.ActorSystem
import pl.edu.bit.membership.recovery.writemodel.rest.AuthToken
import pl.edu.bit.membership.recovery.writemodel.rest.auth.Protocol._
import pl.edu.bit.membership.recovery.writemodel.rest.util.HttpResponseToResponseConversions._
import spray.client.pipelining._
import spray.httpx.SprayJsonSupport._

import scala.concurrent.Future

/**
  * @author Wojciech Milewski
  */
class AuthConnection(address: String)(implicit system: ActorSystem) {
   val TOKEN_HEADER = "knbit-aa-auth"

  def login(email: String, password: String): Future[AuthToken] = {
    import system.dispatcher
    val pipeline = sendReceive

    pipeline(Post(s"$address/login", LoginRequestJson(email, password))) map {
      case reason if reason.status.isSuccess => reason.as[LoginResponseJson] match {
        case Some(LoginResponseJson(token, _)) => AuthToken(token)
        case None => throw new IllegalArgumentException(s"Login error: email = $email  &&  password = $password")
      }
      case reason => throw new IllegalArgumentException(s"Login error: email = $email  &&  password = $password")
    }

  }

 }
