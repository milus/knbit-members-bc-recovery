package pl.edu.bit.membership.recovery.writemodel.rest.membership

import akka.actor.ActorSystem
import pl.edu.bit.membership.recovery.readmodel.db.UserDetailsEntity
import pl.edu.bit.membership.recovery.readmodel.valueobject.UserId
import pl.edu.bit.membership.recovery.writemodel.rest.AuthToken
import pl.edu.bit.membership.recovery.writemodel.rest.membership.Protocol._
import spray.client.pipelining._
import spray.http.HttpResponse

import scala.concurrent.Future

/**
 * @author Wojciech Milewski
 */
class MembershipConnection(address: String)(implicit system: ActorSystem) {
  val TOKEN_HEADER = "knbit-aa-auth"

  def submitDeclarationFor(entity: UserDetailsEntity)(implicit authToken: AuthToken): Future[HttpResponse] = {
    import system.dispatcher

    val pipeline = addHeader(TOKEN_HEADER, authToken.value) ~> sendReceive

    pipeline(Post(s"$address/declaration/submit", SubmitDeclarationRequestJson(
      entity.userId.id,
      entity.email.value,
      entity.firstName,
      entity.lastName,
      entity.departmentName,
      entity.indexNumber.toInt,
      entity.startOfStudiesYear.toInt
    )))
  }

  def accept(userId: UserId)(implicit authToken: AuthToken): Future[HttpResponse] = {
    import system.dispatcher
    val pipeline = addHeader(TOKEN_HEADER, authToken.value) ~> sendReceive

    pipeline(Post(s"$address/declaration/accept", AcceptDeclarationRequestJson(userId.id)))
  }

  def reject(userId: UserId)(implicit authToken: AuthToken): Future[HttpResponse] = {
    import system.dispatcher
    val pipeline = addHeader(TOKEN_HEADER, authToken.value) ~> sendReceive

    pipeline(Post(s"$address/declaration/reject", RejectDeclarationRequestJson(userId.id)))
  }

}
