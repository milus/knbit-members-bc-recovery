package pl.edu.bit.membership.recovery.readmodel.valueobject

/**
 * @author Wojciech Milewski
 */
case class UserId(id: String) extends AnyVal
