package pl.edu.bit.membership.recovery.writemodel

import com.typesafe.scalalogging.StrictLogging
import pl.edu.bit.membership.recovery.readmodel.db.{UserDetailsEntity, UserDetailsState}
import pl.edu.bit.membership.recovery.writemodel.rest.AuthToken
import pl.edu.bit.membership.recovery.writemodel.rest.membership.MembershipConnection
import spray.http.HttpResponse

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * @author Wojciech Milewski
 */
class MembershipRecoveryService(implicit membershipConnection: MembershipConnection) extends StrictLogging {

  def recover(entity: UserDetailsEntity)(implicit authToken: AuthToken): Future[Unit] = {

    def acceptOrRejectEntity: (HttpResponse) => Future[HttpResponse] = {
      case response if response.status.isSuccess =>
        if (entity.state == UserDetailsState.ACCEPTED)
          membershipConnection.accept(entity.userId)
        else if (entity.state == UserDetailsState.REJECTED)
          membershipConnection.reject(entity.userId)
        else Future.successful(response)
      case response => Future.successful(response)

    }

    def throwOnFailedResponse: (HttpResponse) => Unit = {
      case response if response.status.isSuccess =>
        logger.info(s"Recovery successful for $entity")
      case response => throw new scala.RuntimeException(s"code: ${response.status.value}\nvalue: ${response.entity.asString}")
    }


    membershipConnection.submitDeclarationFor(entity)
      .flatMap(acceptOrRejectEntity)
      .map(throwOnFailedResponse)
  }
}
