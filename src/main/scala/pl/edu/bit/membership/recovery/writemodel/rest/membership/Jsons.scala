package pl.edu.bit.membership.recovery.writemodel.rest.membership

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

/**
 * @author Wojciech Milewski
 */
sealed trait RequestJson

case class SubmitDeclarationRequestJson(
                                         userId: String,
                                         email: String,
                                         firstName: String,
                                         lastName: String,
                                         departmentName: String,
                                         indexNumber: Int,
                                         startOfStudiesYear: Int) extends RequestJson
case class AcceptDeclarationRequestJson(
                                         userId: String
                                         ) extends RequestJson
case class RejectDeclarationRequestJson(
                                         userId: String
                                         ) extends RequestJson

object Protocol extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val submitDeclarationRequest = jsonFormat7(SubmitDeclarationRequestJson)
  implicit val acceptDeclarationRequest = jsonFormat1(AcceptDeclarationRequestJson)
  implicit val rejectDeclarationRequest = jsonFormat1(RejectDeclarationRequestJson)
}