package pl.edu.bit.membership.recovery.writemodel.rest.auth

import spray.json.DefaultJsonProtocol

/**
 * @author Wojciech Milewski
 */
sealed trait RequestJson

case class LoginRequestJson(email: String, password: String) extends RequestJson

sealed trait ResponseJson
case class LoginResponseJson(token: String, userId: String) extends ResponseJson

object Protocol extends DefaultJsonProtocol {
  implicit val loginRequest = jsonFormat2(LoginRequestJson)
  implicit val loginResponse = jsonFormat2(LoginResponseJson)
}