package pl.edu.bit.membership.recovery.readmodel

import pl.edu.bit.membership.recovery.readmodel.db.{UserDetailsDao, UserDetailsEntity}

import scala.concurrent.Future

/**
 * @author Wojciech Milewski
 */
class UserDetailsProvider(implicit dao: UserDetailsDao) {

  def getAll: Future[Iterator[UserDetailsEntity]] = dao.loadAll()
}
