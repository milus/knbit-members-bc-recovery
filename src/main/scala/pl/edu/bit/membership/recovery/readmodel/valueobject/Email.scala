package pl.edu.bit.membership.recovery.readmodel.valueobject

/**
 * @author Wojciech Milewski
 */
case class Email(value: String) extends AnyVal
