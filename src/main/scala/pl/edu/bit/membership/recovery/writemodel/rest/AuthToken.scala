package pl.edu.bit.membership.recovery.writemodel.rest

/**
 * @author Wojciech Milewski
 */
case class AuthToken(value: String) extends AnyVal
