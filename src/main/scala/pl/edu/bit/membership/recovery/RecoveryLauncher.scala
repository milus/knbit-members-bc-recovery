package pl.edu.bit.membership.recovery

import akka.actor.ActorSystem
import com.mongodb.casbah.Imports._
import com.typesafe.scalalogging.StrictLogging
import pl.edu.bit.membership.recovery.readmodel.UserDetailsProvider
import pl.edu.bit.membership.recovery.readmodel.db.{UserDetailsEntity, UserDetailsDao}
import pl.edu.bit.membership.recovery.writemodel.MembershipRecoveryService
import pl.edu.bit.membership.recovery.writemodel.rest.AuthToken
import pl.edu.bit.membership.recovery.writemodel.rest.auth.AuthConnection
import pl.edu.bit.membership.recovery.writemodel.rest.membership.MembershipConnection

import scala.concurrent.Future
import scala.util.{Failure, Success}

/**
 * @author Wojciech Milewski
 */
object RecoveryLauncher extends App with StrictLogging {

  verifyArgs()

  val mongoAddress = args(0)
  val databaseName = args(1)
  val email = args(2)
  val password = args(3)
  val authAddress = args(4)
  val membershipAddress = args(5)

  implicit val system = ActorSystem("RecoveryApp")

  implicit val mongoCollection = MongoClient(mongoAddress)(databaseName)("userdetailsentity")
  implicit val userDetailsDao = new UserDetailsDao

  val provider: UserDetailsProvider = new UserDetailsProvider
  val authConnection: AuthConnection = new AuthConnection(authAddress)
  implicit val membershipConnection = new MembershipConnection(membershipAddress)

  val recoveryService = new MembershipRecoveryService

  import system.dispatcher

  authConnection.login(email, password)
    .flatMap(provideUserData)
    .flatMap(recoverEachUser)
    .onComplete {
    case Success(_) => logger.info("Finished recovery")
    case Failure(e) => logger.error("Recovery failed", e)
  }

  def provideUserData(token: AuthToken): Future[(AuthToken, Iterator[UserDetailsEntity])] = provider.getAll.map((token, _))

  def recoverEachUser: ((AuthToken, Iterator[UserDetailsEntity])) => Future[Iterator[Unit]] = {
    pair => {
      implicit val token = pair._1
      val iterator = pair._2
      Future.traverse(iterator)(entity => recoveryService.recover(entity))
    }
  }

  def verifyArgs() = {
    if (args.length < 6) {
      println("Required arguments: mongoAddress databaseName email password authAddress membershipAddress")
      System.exit(1)
    }
  }


}
